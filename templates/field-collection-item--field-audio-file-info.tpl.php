<div class="episode-language">
    <?php print render($content['field_audio_language']); ?>
    <a href="<?php print strip_tags(render($content['field_audio_file'])); ?>" class="dl-icon" download>
      Download
    </a>
</div>
<div class="episode-link-audio-file">
  <audio controls>
    <source src="<?php print strip_tags(render($content['field_audio_file'])); ?>" type="audio/mpeg">
    Your browser does not support the audio element.
  </audio>
</div>