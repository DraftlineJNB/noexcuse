<script>
  var themepath = '<?php echo drupal_get_path('theme', 'theme_carlingblacklabel2019'); ?>';
</script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId: '411661346007234',
      cookie: true,
      xfbml: true,
      version: 'v2.0'
    });

    FB.AppEvents.logPageView();

  };
</script>

<section class="general-no-excuse">
  <nav class="block-nav">
    <div class="container">
      <div class="box-logo">
        <img src="<?php echo drupal_get_path('theme', 'theme_carlingblacklabel2019'); ?>/front-dev/images/brand/logo-excuse.svg">
      </div>
    </div>
  </nav>

  <article class="agegate__content">
    <div class="agegate__image">
      <div class="agegate--image__xs">
        <img src="<?php echo drupal_get_path('theme', 'theme_carlingblacklabel2019'); ?>/front-dev/images/handMob.jpg" alt="Renemwall">
      </div>
      <div class="agegate--image__md">
        <img src="<?php echo drupal_get_path('theme', 'theme_carlingblacklabel2019'); ?>/front-dev/images/handDesktop.jpg" alt="PrincipalImage">
      </div>
    </div>
    <div class="agegate__body">
      <div class="container slide--bride">
        <h2 class="desc">BRIDE<strong>ARMOUR</strong></h2>
        <h1 class="title"><strong>THE DRESS WE WISH WE NEVER HAD TO MAKE</strong></h1>
        <h2 class="sub-title"><strong>GLOBALLY, 1 IN 3 WOMEN WILL EXPERIENCE INTIMATE PARTNER VIOLENCE.</strong></h2>
      </div>
    </div>
  </article>

  <div class="wrapper-block">

    <div class="container container-bottom topper">

      <div class="row">
        <div class="col-xs-12 col-md-6">
          <article class="block-info">

            <div class="row box-row box-negative">
              <div class="box-item col-xs-6 col-md-6">
                <!-- <h1 class="title">
                                    <span class="color-red">RE-BEL <span class="color-white">AGAINST THE IDEA</span></span>
                                    <span class="color-red">THAT YOU CAN'T HELP</span>
                                </h1>   -->

                <div class="block-animated-text">
                  <div class="box-owl box-desk">
                    <div class="item">
                      <p><span class="color-red">SIGN OUR</span> CHANGE.ORG PETITION</p>
                    </div>
                    <!-- others items to animate
                                        <div class="item">
                                            <p><span class="color-red">Re-commit</span> TO HELPING IN THE FIGHT AGAINST GBV</p>
                                        </div>-->
                  </div>

                  <div class="box-owl box-mobile">
                    <div class="item">
                      <p><span class="color-red">SIGN OUR</span> CHANGE.ORG PETITION</p>
                    </div>
                    <!-- others items to animate
                                        <div class="item">
                                            <p><span class="color-red">Re-commit</span> TO HELPING IN THE FIGHT AGAINST GBV</p>
                                        </div>-->
                  </div>
                </div>
              </div>


              <div class="box-item col-xs-6 col-md-6">
                <div class="block-min-text">

                </div>

                <div class="block-nav-lg">
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active first"><a href="#getmypost" aria-controls="getmypost" role="tab" data-toggle="tab" aria-expanded="true">SIGN THE PETITION</a></li>
                    <li role="presentation" class="last"><a class="continuetosite" href="#continuetosite" aria-controls="continuetosite" role="tab" data-toggle="tab">CONTINUE TO SITE</a></li>
                  </ul>
                </div>
              </div>
            </div>

          </article>
        </div>

        <div class="col-xs-12 col-md-6">
          <div class="box-nav">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active first"><a href="#getmypost" aria-controls="getmypost" role="tab" data-toggle="tab" aria-expanded="true">SIGN THE PETITION</a></li>
              <li role="presentation" class="last"><a class="continuetosite" href="#continuetosite" aria-controls="continuetosite" role="tab" data-toggle="tab">CONTINUE TO SITE</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="getmypost">

                <!--- Formulario 2-->
                <form accept-charset="UTF-8" action="/agegate" id="post-form" method="post">
                  <div class="row">
                    <div class="col-md-8 box-form">
                      <div class="wrapper-datos">
                        <div class="form-item form-item-firstname form-type-textfield form-group first-item">
                          <input class="form-control form-text required" id="edit-firstname" maxlength="128" name="firstname" placeholder="FIRST NAME" size="60" td-type="firstname" type="text" value=""></input>
                        </div>

                        <div class="form-item form-item-cellphone form-type-textfield form-group last-item">
                          <input class="form-control form-text required" id="edit-cellphone" maxlength="128" name="cellphone" placeholder="CELLPHONE NUMBER" size="60" td-type="cellphone" type="text" value=""></input>
                        </div>
                      </div>

                      <div class="form-item form-item-surname form-type-textfield form-group">
                        <input class="form-control form-text required" id="edit-surname" maxlength="128" name="surname" placeholder="SURNAME" size="60" td-type="lastname" type="text" value=""></input>
                      </div>
                      <div class="form-item form-item-selected form-type-select form-group">
                        <select class="form-control form-select" id="edit-selected" name="selected">
                          <option value="">
                            SELECT YOUR COUNTRY
                          </option>
                          <option value="ZA">
                            South Africa
                          </option>
                          <option value="AF">
                            Afghanistan
                          </option>
                          <option value="AL">
                            Albania
                          </option>
                          <option value="DZ">
                            Algeria
                          </option>
                          <option value="AS">
                            American Samoa
                          </option>
                          <option value="AD">
                            Andorra
                          </option>
                          <option value="AO">
                            Angola
                          </option>
                          <option value="AI">
                            Anguilla
                          </option>
                          <option value="AQ">
                            Antarctica
                          </option>
                          <option value="AG">
                            Antigua and Barbuda
                          </option>
                          <option value="AR">
                            Argentina
                          </option>
                          <option value="AM">
                            Armenia
                          </option>
                          <option value="AW">
                            Aruba
                          </option>
                          <option value="AU">
                            Australia
                          </option>
                          <option value="AT">
                            Austria
                          </option>
                          <option value="AZ">
                            Azerbaijan
                          </option>
                          <option value="BS">
                            Bahamas
                          </option>
                          <option value="BH">
                            Bahrain
                          </option>
                          <option value="BD">
                            Bangladesh
                          </option>
                          <option value="BB">
                            Barbados
                          </option>
                          <option value="BY">
                            Belarus
                          </option>
                          <option value="BE">
                            Belgium
                          </option>
                          <option value="BZ">
                            Belize
                          </option>
                          <option value="BJ">
                            Benin
                          </option>
                          <option value="BM">
                            Bermuda
                          </option>
                          <option value="BT">
                            Bhutan
                          </option>
                          <option value="BO">
                            Bolivia
                          </option>
                          <option value="BA">
                            Bosnia and Herzegovina
                          </option>
                          <option value="BW">
                            Botswana
                          </option>
                          <option value="BV">
                            Bouvet Island
                          </option>
                          <option value="BR">
                            Brazil
                          </option>
                          <option value="IO">
                            British Indian Ocean Territory
                          </option>
                          <option value="BN">
                            Brunei Darussalam
                          </option>
                          <option value="BG">
                            Bulgaria
                          </option>
                          <option value="BF">
                            Burkina Faso
                          </option>
                          <option value="BI">
                            Burundi
                          </option>
                          <option value="KH">
                            Cambodia
                          </option>
                          <option value="CM">
                            Cameroon
                          </option>
                          <option value="CA">
                            Canada
                          </option>
                          <option value="CV">
                            Cape Verde
                          </option>
                          <option value="KY">
                            Cayman Islands
                          </option>
                          <option value="CF">
                            Central African Republic
                          </option>
                          <option value="TD">
                            Chad
                          </option>
                          <option value="CL">
                            Chile
                          </option>
                          <option value="CN">
                            China
                          </option>
                          <option value="CX">
                            Christmas Island
                          </option>
                          <option value="CC">
                            Cocos (Keeling) Islands
                          </option>
                          <option value="CO">
                            Colombia
                          </option>
                          <option value="KM">
                            Comoros
                          </option>
                          <option value="CG">
                            Congo
                          </option>
                          <option value="CD">
                            Congo, the Democratic Republic of the
                          </option>
                          <option value="CK">
                            Cook Islands
                          </option>
                          <option value="CR">
                            Costa Rica
                          </option>
                          <option value="CI">
                            Cote D'Ivoire
                          </option>
                          <option value="HR">
                            Croatia
                          </option>
                          <option value="CU">
                            Cuba
                          </option>
                          <option value="CY">
                            Cyprus
                          </option>
                          <option value="CZ">
                            Czech Republic
                          </option>
                          <option value="DK">
                            Denmark
                          </option>
                          <option value="DJ">
                            Djibouti
                          </option>
                          <option value="DM">
                            Dominica
                          </option>
                          <option value="DO">
                            Dominican Republic
                          </option>
                          <option value="EC">
                            Ecuador
                          </option>
                          <option value="EG">
                            Egypt
                          </option>
                          <option value="SV">
                            El Salvador
                          </option>
                          <option value="GQ">
                            Equatorial Guinea
                          </option>
                          <option value="ER">
                            Eritrea
                          </option>
                          <option value="EE">
                            Estonia
                          </option>
                          <option value="ET">
                            Ethiopia
                          </option>
                          <option value="FK">
                            Falkland Islands (Malvinas)
                          </option>
                          <option value="FO">
                            Faroe Islands
                          </option>
                          <option value="FJ">
                            Fiji
                          </option>
                          <option value="FI">
                            Finland
                          </option>
                          <option value="FR">
                            France
                          </option>
                          <option value="GF">
                            French Guiana
                          </option>
                          <option value="PF">
                            French Polynesia
                          </option>
                          <option value="TF">
                            French Southern Territories
                          </option>
                          <option value="GA">
                            Gabon
                          </option>
                          <option value="GM">
                            Gambia
                          </option>
                          <option value="GE">
                            Georgia
                          </option>
                          <option value="DE">
                            Germany
                          </option>
                          <option value="GH">
                            Ghana
                          </option>
                          <option value="GI">
                            Gibraltar
                          </option>
                          <option value="GR">
                            Greece
                          </option>
                          <option value="GL">
                            Greenland
                          </option>
                          <option value="GD">
                            Grenada
                          </option>
                          <option value="GP">
                            Guadeloupe
                          </option>
                          <option value="GU">
                            Guam
                          </option>
                          <option value="GT">
                            Guatemala
                          </option>
                          <option value="GN">
                            Guinea
                          </option>
                          <option value="GW">
                            Guinea-Bissau
                          </option>
                          <option value="GY">
                            Guyana
                          </option>
                          <option value="HT">
                            Haiti
                          </option>
                          <option value="HM">
                            Heard Island and Mcdonald Islands
                          </option>
                          <option value="VA">
                            Holy See (Vatican City State)
                          </option>
                          <option value="HN">
                            Honduras
                          </option>
                          <option value="HK">
                            Hong Kong
                          </option>
                          <option value="HU">
                            Hungary
                          </option>
                          <option value="IS">
                            Iceland
                          </option>
                          <option value="IN">
                            India
                          </option>
                          <option value="ID">
                            Indonesia
                          </option>
                          <option value="IR">
                            Iran, Islamic Republic of
                          </option>
                          <option value="IQ">
                            Iraq
                          </option>
                          <option value="IE">
                            Ireland
                          </option>
                          <option value="IL">
                            Israel
                          </option>
                          <option value="IT">
                            Italy
                          </option>
                          <option value="JM">
                            Jamaica
                          </option>
                          <option value="JP">
                            Japan
                          </option>
                          <option value="JO">
                            Jordan
                          </option>
                          <option value="KZ">
                            Kazakhstan
                          </option>
                          <option value="KE">
                            Kenya
                          </option>
                          <option value="KI">
                            Kiribati
                          </option>
                          <option value="KP">
                            Korea, Democratic People's Republic of
                          </option>
                          <option value="KR">
                            Korea, Republic of
                          </option>
                          <option value="KW">
                            Kuwait
                          </option>
                          <option value="KG">
                            Kyrgyzstan
                          </option>
                          <option value="LA">
                            Lao People's Democratic Republic
                          </option>
                          <option value="LV">
                            Latvia
                          </option>
                          <option value="LB">
                            Lebanon
                          </option>
                          <option value="LS">
                            Lesotho
                          </option>
                          <option value="LR">
                            Liberia
                          </option>
                          <option value="LY">
                            Libyan Arab Jamahiriya
                          </option>
                          <option value="LI">
                            Liechtenstein
                          </option>
                          <option value="LT">
                            Lithuania
                          </option>
                          <option value="LU">
                            Luxembourg
                          </option>
                          <option value="MO">
                            Macao
                          </option>
                          <option value="MK">
                            Macedonia, the Former Yugoslav Republic of
                          </option>
                          <option value="MG">
                            Madagascar
                          </option>
                          <option value="MW">
                            Malawi
                          </option>
                          <option value="MY">
                            Malaysia
                          </option>
                          <option value="MV">
                            Maldives
                          </option>
                          <option value="ML">
                            Mali
                          </option>
                          <option value="MT">
                            Malta
                          </option>
                          <option value="MH">
                            Marshall Islands
                          </option>
                          <option value="MQ">
                            Martinique
                          </option>
                          <option value="MR">
                            Mauritania
                          </option>
                          <option value="MU">
                            Mauritius
                          </option>
                          <option value="YT">
                            Mayotte
                          </option>
                          <option value="MX">
                            Mexico
                          </option>
                          <option value="FM">
                            Micronesia, Federated States of
                          </option>
                          <option value="MD">
                            Moldova, Republic of
                          </option>
                          <option value="MC">
                            Monaco
                          </option>
                          <option value="MN">
                            Mongolia
                          </option>
                          <option value="MS">
                            Montserrat
                          </option>
                          <option value="MA">
                            Morocco
                          </option>
                          <option value="MZ">
                            Mozambique
                          </option>
                          <option value="MM">
                            Myanmar
                          </option>
                          <option value="NA">
                            Namibia
                          </option>
                          <option value="NR">
                            Nauru
                          </option>
                          <option value="NP">
                            Nepal
                          </option>
                          <option value="NL">
                            Netherlands
                          </option>
                          <option value="AN">
                            Netherlands Antilles
                          </option>
                          <option value="NC">
                            New Caledonia
                          </option>
                          <option value="NZ">
                            New Zealand
                          </option>
                          <option value="NI">
                            Nicaragua
                          </option>
                          <option value="NE">
                            Niger
                          </option>
                          <option value="NG">
                            Nigeria
                          </option>
                          <option value="NU">
                            Niue
                          </option>
                          <option value="NF">
                            Norfolk Island
                          </option>
                          <option value="MP">
                            Northern Mariana Islands
                          </option>
                          <option value="NO">
                            Norway
                          </option>
                          <option value="OM">
                            Oman
                          </option>
                          <option value="PK">
                            Pakistan
                          </option>
                          <option value="PW">
                            Palau
                          </option>
                          <option value="PS">
                            Palestinian Territory, Occupied
                          </option>
                          <option value="PA">
                            Panama
                          </option>
                          <option value="PG">
                            Papua New Guinea
                          </option>
                          <option value="PY">
                            Paraguay
                          </option>
                          <option value="PE">
                            Peru
                          </option>
                          <option value="PH">
                            Philippines
                          </option>
                          <option value="PN">
                            Pitcairn
                          </option>
                          <option value="PL">
                            Poland
                          </option>
                          <option value="PT">
                            Portugal
                          </option>
                          <option value="PR">
                            Puerto Rico
                          </option>
                          <option value="QA">
                            Qatar
                          </option>
                          <option value="RE">
                            Reunion
                          </option>
                          <option value="RO">
                            Romania
                          </option>
                          <option value="RU">
                            Russian Federation
                          </option>
                          <option value="RW">
                            Rwanda
                          </option>
                          <option value="SH">
                            Saint Helena
                          </option>
                          <option value="KN">
                            Saint Kitts and Nevis
                          </option>
                          <option value="LC">
                            Saint Lucia
                          </option>
                          <option value="PM">
                            Saint Pierre and Miquelon
                          </option>
                          <option value="VC">
                            Saint Vincent and the Grenadines
                          </option>
                          <option value="WS">
                            Samoa
                          </option>
                          <option value="SM">
                            San Marino
                          </option>
                          <option value="ST">
                            Sao Tome and Principe
                          </option>
                          <option value="SA">
                            Saudi Arabia
                          </option>
                          <option value="SN">
                            Senegal
                          </option>
                          <option value="CS">
                            Serbia and Montenegro
                          </option>
                          <option value="SC">
                            Seychelles
                          </option>
                          <option value="SL">
                            Sierra Leone
                          </option>
                          <option value="SG">
                            Singapore
                          </option>
                          <option value="SK">
                            Slovakia
                          </option>
                          <option value="SI">
                            Slovenia
                          </option>
                          <option value="SB">
                            Solomon Islands
                          </option>
                          <option value="SO">
                            Somalia
                          </option>
                          <option value="ZA">
                            South Africa
                          </option>
                          <option value="GS">
                            South Georgia and the South Sandwich Islands
                          </option>
                          <option value="ES">
                            Spain
                          </option>
                          <option value="LK">
                            Sri Lanka
                          </option>
                          <option value="SD">
                            Sudan
                          </option>
                          <option value="SR">
                            Suriname
                          </option>
                          <option value="SJ">
                            Svalbard and Jan Mayen
                          </option>
                          <option value="SZ">
                            Swaziland
                          </option>
                          <option value="SE">
                            Sweden
                          </option>
                          <option value="CH">
                            Switzerland
                          </option>
                          <option value="SY">
                            Syrian Arab Republic
                          </option>
                          <option value="TW">
                            Taiwan, Province of China
                          </option>
                          <option value="TJ">
                            Tajikistan
                          </option>
                          <option value="TZ">
                            Tanzania, United Republic of
                          </option>
                          <option value="TH">
                            Thailand
                          </option>
                          <option value="TL">
                            Timor-Leste
                          </option>
                          <option value="TG">
                            Togo
                          </option>
                          <option value="TK">
                            Tokelau
                          </option>
                          <option value="TO">
                            Tonga
                          </option>
                          <option value="TT">
                            Trinidad and Tobago
                          </option>
                          <option value="TN">
                            Tunisia
                          </option>
                          <option value="TR">
                            Turkey
                          </option>
                          <option value="TM">
                            Turkmenistan
                          </option>
                          <option value="TC">
                            Turks and Caicos Islands
                          </option>
                          <option value="TV">
                            Tuvalu
                          </option>
                          <option value="UG">
                            Uganda
                          </option>
                          <option value="UA">
                            Ukraine
                          </option>
                          <option value="AE">
                            United Arab Emirates
                          </option>
                          <option value="GB">
                            United Kingdom
                          </option>
                          <option value="US">
                            United States
                          </option>
                          <option value="UM">
                            United States Minor Outlying Islands
                          </option>
                          <option value="UY">
                            Uruguay
                          </option>
                          <option value="UZ">
                            Uzbekistan
                          </option>
                          <option value="VU">
                            Vanuatu
                          </option>
                          <option value="VE">
                            Venezuela
                          </option>
                          <option value="VN">
                            Viet Nam
                          </option>
                          <option value="VG">
                            Virgin Islands, British
                          </option>
                          <option value="VI">
                            Virgin Islands, U.s.
                          </option>
                          <option value="WF">
                            Wallis and Futuna
                          </option>
                          <option value="EH">
                            Western Sahara
                          </option>
                          <option value="YE">
                            Yemen
                          </option>
                          <option value="ZM">
                            Zambia
                          </option>
                          <option value="ZW">
                            Zimbabwe
                          </option>
                        </select>
                      </div>

                      <div class="wrapper-date">
                        <div class="form-item form-item-dayofbirth form-type-select form-group first-item">
                          <select class="form-control form-select" id="edit-dayofbirth" name="dayofbirth">
                            <option value="">
                              DAY
                            </option>
                            <option value="01">
                              01
                            </option>
                            <option value="02">
                              02
                            </option>
                            <option value="03">
                              03
                            </option>
                            <option value="04">
                              04
                            </option>
                            <option value="05">
                              05
                            </option>
                            <option value="06">
                              06
                            </option>
                            <option value="07">
                              07
                            </option>
                            <option value="08">
                              08
                            </option>
                            <option value="09">
                              09
                            </option>
                            <option value="10">
                              10
                            </option>
                            <option value="11">
                              11
                            </option>
                            <option value="12">
                              12
                            </option>
                            <option value="13">
                              13
                            </option>
                            <option value="14">
                              14
                            </option>
                            <option value="15">
                              15
                            </option>
                            <option value="16">
                              16
                            </option>
                            <option value="17">
                              17
                            </option>
                            <option value="18">
                              18
                            </option>
                            <option value="19">
                              19
                            </option>
                            <option value="20">
                              20
                            </option>
                            <option value="21">
                              21
                            </option>
                            <option value="22">
                              22
                            </option>
                            <option value="23">
                              23
                            </option>
                            <option value="24">
                              24
                            </option>
                            <option value="25">
                              25
                            </option>
                            <option value="26">
                              26
                            </option>
                            <option value="27">
                              27
                            </option>
                            <option value="28">
                              28
                            </option>
                            <option value="29">
                              29
                            </option>
                            <option value="30">
                              30
                            </option>
                            <option value="31">
                              31
                            </option>
                          </select>
                        </div>
                        <div class="form-item form-item-monthofbirth form-type-select form-group">
                          <select class="form-control form-select" id="edit-monthofbirth" name="monthofbirth">
                            <option value="">
                              MONTH
                            </option>
                            <option value="01">
                              01
                            </option>
                            <option value="02">
                              02
                            </option>
                            <option value="03">
                              03
                            </option>
                            <option value="04">
                              04
                            </option>
                            <option value="05">
                              05
                            </option>
                            <option value="06">
                              06
                            </option>
                            <option value="07">
                              07
                            </option>
                            <option value="08">
                              08
                            </option>
                            <option value="09">
                              09
                            </option>
                            <option value="10">
                              10
                            </option>
                            <option value="11">
                              11
                            </option>
                            <option value="12">
                              12
                            </option>
                          </select>
                        </div>
                        <div class="form-item form-item-yearofbirth form-type-select form-group last-item">
                          <select class="form-control form-select" id="edit-yearofbirth" name="yearofbirth">
                            <option value="">
                              YEAR
                            </option>
                            <?php $year = date("Y"); ?>
                            <?php for ($i = 0; $i < 60; $i++) { ?>
                              <option value="<?php echo $year - 18 - $i; ?>">
                                <?php echo $year - 18 - $i; ?>
                              </option>
                            <?php } ?>

                          </select>
                        </div>
                      </div>

                      <div class="block-btn">
                        <button class="btn btn-primary form-submit" id="edit-submit" name="op" type="submit" value="SUBMIT">SUBMIT</button>
                      </div>
                    </div>

                    <div class="col-md-4 box-bottom">
                      <div class="form-item form-item-remember form-type-checkbox checkbox">
                        <input class="form-checkbox" id="edit-remember" name="remember" type="checkbox" value="1"></input>
                        <label class="control-label" for="edit-remember"><span>REMEMBER ME</span></label>
                      </div>
                      <div class="txt_dontstay">
                        <p>Do not stay connected if the computer is shared with people under the age of 18. By clicking here and facebook connect, you’ll accept the use of cookies.</p>
                        <p><a href="">terms & conditions</a> and <a href="">privacy policy</a></p>
                      </div>
                    </div>
                  </div>

                  <input name="form_build_id" type="hidden" value="form-jMCIg_1h4ZRpifjgw145iPA_m-pgN4SijSM1r8SuZvc"></input>
                  <input name="form_id" type="hidden" value="post_form"></input>
                </form>
              </div>

              <div role="tabpanel" class="tab-pane" id="continuetosite">
                <form accept-charset="UTF-8" action="/agegate" id="agegate-form" method="post">
                  <div class="row box-form-row">
                    <div class="box-form col-md-8">
                      <div class="form-item form-item-country form-type-select form-group">
                        <select class="form-control form-select" id="edit-country" name="country">
                          <option value="">
                            SELECT YOUR COUNTRY
                          </option>
                          <option value="ZA">
                            South Africa
                          </option>
                          <option value="AF">
                            Afghanistan
                          </option>
                          <option value="AL">
                            Albania
                          </option>
                          <option value="DZ">
                            Algeria
                          </option>
                          <option value="AS">
                            American Samoa
                          </option>
                          <option value="AD">
                            Andorra
                          </option>
                          <option value="AO">
                            Angola
                          </option>
                          <option value="AI">
                            Anguilla
                          </option>
                          <option value="AQ">
                            Antarctica
                          </option>
                          <option value="AG">
                            Antigua and Barbuda
                          </option>
                          <option value="AR">
                            Argentina
                          </option>
                          <option value="AM">
                            Armenia
                          </option>
                          <option value="AW">
                            Aruba
                          </option>
                          <option value="AU">
                            Australia
                          </option>
                          <option value="AT">
                            Austria
                          </option>
                          <option value="AZ">
                            Azerbaijan
                          </option>
                          <option value="BS">
                            Bahamas
                          </option>
                          <option value="BH">
                            Bahrain
                          </option>
                          <option value="BD">
                            Bangladesh
                          </option>
                          <option value="BB">
                            Barbados
                          </option>
                          <option value="BY">
                            Belarus
                          </option>
                          <option value="BE">
                            Belgium
                          </option>
                          <option value="BZ">
                            Belize
                          </option>
                          <option value="BJ">
                            Benin
                          </option>
                          <option value="BM">
                            Bermuda
                          </option>
                          <option value="BT">
                            Bhutan
                          </option>
                          <option value="BO">
                            Bolivia
                          </option>
                          <option value="BA">
                            Bosnia and Herzegovina
                          </option>
                          <option value="BW">
                            Botswana
                          </option>
                          <option value="BV">
                            Bouvet Island
                          </option>
                          <option value="BR">
                            Brazil
                          </option>
                          <option value="IO">
                            British Indian Ocean Territory
                          </option>
                          <option value="BN">
                            Brunei Darussalam
                          </option>
                          <option value="BG">
                            Bulgaria
                          </option>
                          <option value="BF">
                            Burkina Faso
                          </option>
                          <option value="BI">
                            Burundi
                          </option>
                          <option value="KH">
                            Cambodia
                          </option>
                          <option value="CM">
                            Cameroon
                          </option>
                          <option value="CA">
                            Canada
                          </option>
                          <option value="CV">
                            Cape Verde
                          </option>
                          <option value="KY">
                            Cayman Islands
                          </option>
                          <option value="CF">
                            Central African Republic
                          </option>
                          <option value="TD">
                            Chad
                          </option>
                          <option value="CL">
                            Chile
                          </option>
                          <option value="CN">
                            China
                          </option>
                          <option value="CX">
                            Christmas Island
                          </option>
                          <option value="CC">
                            Cocos (Keeling) Islands
                          </option>
                          <option value="CO">
                            Colombia
                          </option>
                          <option value="KM">
                            Comoros
                          </option>
                          <option value="CG">
                            Congo
                          </option>
                          <option value="CD">
                            Congo, the Democratic Republic of the
                          </option>
                          <option value="CK">
                            Cook Islands
                          </option>
                          <option value="CR">
                            Costa Rica
                          </option>
                          <option value="CI">
                            Cote D'Ivoire
                          </option>
                          <option value="HR">
                            Croatia
                          </option>
                          <option value="CU">
                            Cuba
                          </option>
                          <option value="CY">
                            Cyprus
                          </option>
                          <option value="CZ">
                            Czech Republic
                          </option>
                          <option value="DK">
                            Denmark
                          </option>
                          <option value="DJ">
                            Djibouti
                          </option>
                          <option value="DM">
                            Dominica
                          </option>
                          <option value="DO">
                            Dominican Republic
                          </option>
                          <option value="EC">
                            Ecuador
                          </option>
                          <option value="EG">
                            Egypt
                          </option>
                          <option value="SV">
                            El Salvador
                          </option>
                          <option value="GQ">
                            Equatorial Guinea
                          </option>
                          <option value="ER">
                            Eritrea
                          </option>
                          <option value="EE">
                            Estonia
                          </option>
                          <option value="ET">
                            Ethiopia
                          </option>
                          <option value="FK">
                            Falkland Islands (Malvinas)
                          </option>
                          <option value="FO">
                            Faroe Islands
                          </option>
                          <option value="FJ">
                            Fiji
                          </option>
                          <option value="FI">
                            Finland
                          </option>
                          <option value="FR">
                            France
                          </option>
                          <option value="GF">
                            French Guiana
                          </option>
                          <option value="PF">
                            French Polynesia
                          </option>
                          <option value="TF">
                            French Southern Territories
                          </option>
                          <option value="GA">
                            Gabon
                          </option>
                          <option value="GM">
                            Gambia
                          </option>
                          <option value="GE">
                            Georgia
                          </option>
                          <option value="DE">
                            Germany
                          </option>
                          <option value="GH">
                            Ghana
                          </option>
                          <option value="GI">
                            Gibraltar
                          </option>
                          <option value="GR">
                            Greece
                          </option>
                          <option value="GL">
                            Greenland
                          </option>
                          <option value="GD">
                            Grenada
                          </option>
                          <option value="GP">
                            Guadeloupe
                          </option>
                          <option value="GU">
                            Guam
                          </option>
                          <option value="GT">
                            Guatemala
                          </option>
                          <option value="GN">
                            Guinea
                          </option>
                          <option value="GW">
                            Guinea-Bissau
                          </option>
                          <option value="GY">
                            Guyana
                          </option>
                          <option value="HT">
                            Haiti
                          </option>
                          <option value="HM">
                            Heard Island and Mcdonald Islands
                          </option>
                          <option value="VA">
                            Holy See (Vatican City State)
                          </option>
                          <option value="HN">
                            Honduras
                          </option>
                          <option value="HK">
                            Hong Kong
                          </option>
                          <option value="HU">
                            Hungary
                          </option>
                          <option value="IS">
                            Iceland
                          </option>
                          <option value="IN">
                            India
                          </option>
                          <option value="ID">
                            Indonesia
                          </option>
                          <option value="IR">
                            Iran, Islamic Republic of
                          </option>
                          <option value="IQ">
                            Iraq
                          </option>
                          <option value="IE">
                            Ireland
                          </option>
                          <option value="IL">
                            Israel
                          </option>
                          <option value="IT">
                            Italy
                          </option>
                          <option value="JM">
                            Jamaica
                          </option>
                          <option value="JP">
                            Japan
                          </option>
                          <option value="JO">
                            Jordan
                          </option>
                          <option value="KZ">
                            Kazakhstan
                          </option>
                          <option value="KE">
                            Kenya
                          </option>
                          <option value="KI">
                            Kiribati
                          </option>
                          <option value="KP">
                            Korea, Democratic People's Republic of
                          </option>
                          <option value="KR">
                            Korea, Republic of
                          </option>
                          <option value="KW">
                            Kuwait
                          </option>
                          <option value="KG">
                            Kyrgyzstan
                          </option>
                          <option value="LA">
                            Lao People's Democratic Republic
                          </option>
                          <option value="LV">
                            Latvia
                          </option>
                          <option value="LB">
                            Lebanon
                          </option>
                          <option value="LS">
                            Lesotho
                          </option>
                          <option value="LR">
                            Liberia
                          </option>
                          <option value="LY">
                            Libyan Arab Jamahiriya
                          </option>
                          <option value="LI">
                            Liechtenstein
                          </option>
                          <option value="LT">
                            Lithuania
                          </option>
                          <option value="LU">
                            Luxembourg
                          </option>
                          <option value="MO">
                            Macao
                          </option>
                          <option value="MK">
                            Macedonia, the Former Yugoslav Republic of
                          </option>
                          <option value="MG">
                            Madagascar
                          </option>
                          <option value="MW">
                            Malawi
                          </option>
                          <option value="MY">
                            Malaysia
                          </option>
                          <option value="MV">
                            Maldives
                          </option>
                          <option value="ML">
                            Mali
                          </option>
                          <option value="MT">
                            Malta
                          </option>
                          <option value="MH">
                            Marshall Islands
                          </option>
                          <option value="MQ">
                            Martinique
                          </option>
                          <option value="MR">
                            Mauritania
                          </option>
                          <option value="MU">
                            Mauritius
                          </option>
                          <option value="YT">
                            Mayotte
                          </option>
                          <option value="MX">
                            Mexico
                          </option>
                          <option value="FM">
                            Micronesia, Federated States of
                          </option>
                          <option value="MD">
                            Moldova, Republic of
                          </option>
                          <option value="MC">
                            Monaco
                          </option>
                          <option value="MN">
                            Mongolia
                          </option>
                          <option value="MS">
                            Montserrat
                          </option>
                          <option value="MA">
                            Morocco
                          </option>
                          <option value="MZ">
                            Mozambique
                          </option>
                          <option value="MM">
                            Myanmar
                          </option>
                          <option value="NA">
                            Namibia
                          </option>
                          <option value="NR">
                            Nauru
                          </option>
                          <option value="NP">
                            Nepal
                          </option>
                          <option value="NL">
                            Netherlands
                          </option>
                          <option value="AN">
                            Netherlands Antilles
                          </option>
                          <option value="NC">
                            New Caledonia
                          </option>
                          <option value="NZ">
                            New Zealand
                          </option>
                          <option value="NI">
                            Nicaragua
                          </option>
                          <option value="NE">
                            Niger
                          </option>
                          <option value="NG">
                            Nigeria
                          </option>
                          <option value="NU">
                            Niue
                          </option>
                          <option value="NF">
                            Norfolk Island
                          </option>
                          <option value="MP">
                            Northern Mariana Islands
                          </option>
                          <option value="NO">
                            Norway
                          </option>
                          <option value="OM">
                            Oman
                          </option>
                          <option value="PK">
                            Pakistan
                          </option>
                          <option value="PW">
                            Palau
                          </option>
                          <option value="PS">
                            Palestinian Territory, Occupied
                          </option>
                          <option value="PA">
                            Panama
                          </option>
                          <option value="PG">
                            Papua New Guinea
                          </option>
                          <option value="PY">
                            Paraguay
                          </option>
                          <option value="PE">
                            Peru
                          </option>
                          <option value="PH">
                            Philippines
                          </option>
                          <option value="PN">
                            Pitcairn
                          </option>
                          <option value="PL">
                            Poland
                          </option>
                          <option value="PT">
                            Portugal
                          </option>
                          <option value="PR">
                            Puerto Rico
                          </option>
                          <option value="QA">
                            Qatar
                          </option>
                          <option value="RE">
                            Reunion
                          </option>
                          <option value="RO">
                            Romania
                          </option>
                          <option value="RU">
                            Russian Federation
                          </option>
                          <option value="RW">
                            Rwanda
                          </option>
                          <option value="SH">
                            Saint Helena
                          </option>
                          <option value="KN">
                            Saint Kitts and Nevis
                          </option>
                          <option value="LC">
                            Saint Lucia
                          </option>
                          <option value="PM">
                            Saint Pierre and Miquelon
                          </option>
                          <option value="VC">
                            Saint Vincent and the Grenadines
                          </option>
                          <option value="WS">
                            Samoa
                          </option>
                          <option value="SM">
                            San Marino
                          </option>
                          <option value="ST">
                            Sao Tome and Principe
                          </option>
                          <option value="SA">
                            Saudi Arabia
                          </option>
                          <option value="SN">
                            Senegal
                          </option>
                          <option value="CS">
                            Serbia and Montenegro
                          </option>
                          <option value="SC">
                            Seychelles
                          </option>
                          <option value="SL">
                            Sierra Leone
                          </option>
                          <option value="SG">
                            Singapore
                          </option>
                          <option value="SK">
                            Slovakia
                          </option>
                          <option value="SI">
                            Slovenia
                          </option>
                          <option value="SB">
                            Solomon Islands
                          </option>
                          <option value="SO">
                            Somalia
                          </option>
                          <option value="ZA">
                            South Africa
                          </option>
                          <option value="GS">
                            South Georgia and the South Sandwich Islands
                          </option>
                          <option value="ES">
                            Spain
                          </option>
                          <option value="LK">
                            Sri Lanka
                          </option>
                          <option value="SD">
                            Sudan
                          </option>
                          <option value="SR">
                            Suriname
                          </option>
                          <option value="SJ">
                            Svalbard and Jan Mayen
                          </option>
                          <option value="SZ">
                            Swaziland
                          </option>
                          <option value="SE">
                            Sweden
                          </option>
                          <option value="CH">
                            Switzerland
                          </option>
                          <option value="SY">
                            Syrian Arab Republic
                          </option>
                          <option value="TW">
                            Taiwan, Province of China
                          </option>
                          <option value="TJ">
                            Tajikistan
                          </option>
                          <option value="TZ">
                            Tanzania, United Republic of
                          </option>
                          <option value="TH">
                            Thailand
                          </option>
                          <option value="TL">
                            Timor-Leste
                          </option>
                          <option value="TG">
                            Togo
                          </option>
                          <option value="TK">
                            Tokelau
                          </option>
                          <option value="TO">
                            Tonga
                          </option>
                          <option value="TT">
                            Trinidad and Tobago
                          </option>
                          <option value="TN">
                            Tunisia
                          </option>
                          <option value="TR">
                            Turkey
                          </option>
                          <option value="TM">
                            Turkmenistan
                          </option>
                          <option value="TC">
                            Turks and Caicos Islands
                          </option>
                          <option value="TV">
                            Tuvalu
                          </option>
                          <option value="UG">
                            Uganda
                          </option>
                          <option value="UA">
                            Ukraine
                          </option>
                          <option value="AE">
                            United Arab Emirates
                          </option>
                          <option value="GB">
                            United Kingdom
                          </option>
                          <option value="US">
                            United States
                          </option>
                          <option value="UM">
                            United States Minor Outlying Islands
                          </option>
                          <option value="UY">
                            Uruguay
                          </option>
                          <option value="UZ">
                            Uzbekistan
                          </option>
                          <option value="VU">
                            Vanuatu
                          </option>
                          <option value="VE">
                            Venezuela
                          </option>
                          <option value="VN">
                            Viet Nam
                          </option>
                          <option value="VG">
                            Virgin Islands, British
                          </option>
                          <option value="VI">
                            Virgin Islands, U.s.
                          </option>
                          <option value="WF">
                            Wallis and Futuna
                          </option>
                          <option value="EH">
                            Western Sahara
                          </option>
                          <option value="YE">
                            Yemen
                          </option>
                          <option value="ZM">
                            Zambia
                          </option>
                          <option value="ZW">
                            Zimbabwe
                          </option>
                        </select>
                      </div>

                      <div class="wrapper-date">
                        <div class="form-item form-item-dayofbirth form-type-select form-group first-item">
                          <select class="form-control form-select" id="edit-dayofbirth2" name="dayofbirth">
                            <option value="">
                              DAY
                            </option>
                            <option value="01">
                              01
                            </option>
                            <option value="02">
                              02
                            </option>
                            <option value="03">
                              03
                            </option>
                            <option value="04">
                              04
                            </option>
                            <option value="05">
                              05
                            </option>
                            <option value="06">
                              06
                            </option>
                            <option value="07">
                              07
                            </option>
                            <option value="08">
                              08
                            </option>
                            <option value="09">
                              09
                            </option>
                            <option value="10">
                              10
                            </option>
                            <option value="11">
                              11
                            </option>
                            <option value="12">
                              12
                            </option>
                            <option value="13">
                              13
                            </option>
                            <option value="14">
                              14
                            </option>
                            <option value="15">
                              15
                            </option>
                            <option value="16">
                              16
                            </option>
                            <option value="17">
                              17
                            </option>
                            <option value="18">
                              18
                            </option>
                            <option value="19">
                              19
                            </option>
                            <option value="20">
                              20
                            </option>
                            <option value="21">
                              21
                            </option>
                            <option value="22">
                              22
                            </option>
                            <option value="23">
                              23
                            </option>
                            <option value="24">
                              24
                            </option>
                            <option value="25">
                              25
                            </option>
                            <option value="26">
                              26
                            </option>
                            <option value="27">
                              27
                            </option>
                            <option value="28">
                              28
                            </option>
                            <option value="29">
                              29
                            </option>
                            <option value="30">
                              30
                            </option>
                            <option value="31">
                              31
                            </option>
                          </select>
                        </div>
                        <div class="form-item form-item-monthofbirth form-type-select form-group">
                          <select class="form-control form-select" id="edit-monthofbirth2" name="monthofbirth">
                            <option value="">
                              MONTH
                            </option>
                            <option value="01">
                              01
                            </option>
                            <option value="02">
                              02
                            </option>
                            <option value="03">
                              03
                            </option>
                            <option value="04">
                              04
                            </option>
                            <option value="05">
                              05
                            </option>
                            <option value="06">
                              06
                            </option>
                            <option value="07">
                              07
                            </option>
                            <option value="08">
                              08
                            </option>
                            <option value="09">
                              09
                            </option>
                            <option value="10">
                              10
                            </option>
                            <option value="11">
                              11
                            </option>
                            <option value="12">
                              12
                            </option>
                          </select>
                        </div>
                        <div class="form-item form-item-yearofbirth form-type-select form-group last-item">
                          <select class="form-control form-select" id="edit-yearofbirth2" name="yearofbirth">
                            <option value="">
                              YEAR
                            </option>
                            <?php $year = date("Y"); ?>
                            <?php for ($i = 0; $i < 60; $i++) { ?>
                              <option value="<?php echo $year - 18 - $i; ?>">
                                <?php echo $year - 18 - $i; ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="block-btn">
                        <button class="btn btn-primary form-submit" id="edit-submit" name="op" type="submit" value="SUBMIT">SUBMIT</button>
                      </div>

                      <div class="box-legal">
                        <div class="form-item form-item-remember form-type-checkbox checkbox">
                          <input class="form-checkbox" id="edit-remember" name="remember" type="checkbox" value="1"></input>
                          <label class="control-label" for="edit-remember">
                            <span>REMEMBER ME</span>
                          </label>
                        </div>
                        <div class="txt_dontstay">
                          <p>Do not stay connected if the computer is shared with people under the age of 18. By clicking here and facebook connect, you’ll accept the use of cookies.</p>
                          <p><a href="">terms & conditions</a> and <a href="">privacy policy</a></p>
                        </div>
                      </div>
                    </div>

                    <input name="form_build_id" type="hidden" value="form-JL4x-Yk2T23p67K9Z4-jfxt5sS3key2d_Xc8KgNRyEk">
                    <input name="form_id" type="hidden" value="agegate_form">
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>





<footer class="no-excuse-landing block-footer">
  <div class="container">
    <p class="black"><a href="https://sacoronavirus.co.za/">CORONA VIRUS SOUTH AFRICAN RESOURCE PORTAL</a></p>
    <style type="text/css">
      .black {
        background-color: #171717;
        margin: 0px;
        color: #FFFFFF;
        text-align: center;
      }

      .black a {
        color: #FFFFFF;
        font-size: 16px;
      }
    </style>
    <br><br>
    <div class="row">
      <div class="col-md-4">
        <figure class="block-logo">
          <img src="<?php echo drupal_get_path('theme', 'theme_carlingblacklabel2019'); ?>/front-dev/images/brand/aware-logo-light.png">
        </figure>
      </div>
      <div class="col-md-4">
        <div class="principal-footer">
          <ul>
            <li><a href="">Terms of use</a></li>
            <li><a href="">Privacy policy</a></li>
            <li class="last"><a href="">COOKIE POLICY ANHEUSER BUSCH INBEV © 2018</a></li>
          </ul>

          <!-- <p>ANHEUSER BUSCH INBEV © 2018</p> -->
        </div>
      </div>
      <div class="col-md-4">

        <div class="bottom-footer">
          <p>ENJOY RESPONSIBLY. NOT FOR SALE TO PERSONS UNDER THE AGE OF 18.</p>
        </div>
      </div>
    </div>

  </div>
</footer>

<!-- <div id="preview" class="modal" style="margin: 50px auto 0 auto;background: #cecece; max-height: 410px">
  <div class="imagePH row">
    <img alt="" src="/sites/g/files/phfypu1871/f/POST.jpg" style="width: 100%; padding: 0 16px;" />
  </div>
  <div class="btnsFooter">
    <a id="shareDownload" href="#" aria-label="Download Image">SAVE</a>
    <a id="shareInFacebook" href="#" target="_blank" rel="noopener" aria-label="Share on Facebook">
      <span>Share on Facebook</span>
    </a>

    <a id="shareInTwitter" href="#" target="_blank" rel="noopener" aria-label="Share on Twitter">
      <span>Share on Twitter</span>
    </a>
  </div> -->
<!-- <div class="row">
        <div class="col-md-6">
            <a id="shareInFacebook" class="resp-sharing-button__link" href="#" target="_blank" rel="noopener" aria-label="Share on Facebook">
                <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" /></svg>
                    </div>Share on Facebook
                </div>
            </a>

        </div>
        <div class="col-md-6">
            <a id="shareInTwitter" class="resp-sharing-button__link" href="#" target="_blank" rel="noopener" aria-label="Share on Twitter">
                <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" /></svg>
                    </div>Share on Twitter
                </div>
            </a>
        </div>
        <div class="col-md-6">
            
        </div>
    </div> -->


<!-- </div> -->