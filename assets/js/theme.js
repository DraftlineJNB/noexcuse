/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery(document).ready(function ($) {
  //Landing No excuses
  jQuery("select").niceSelect();
  jQuery(".box-owl .item").css("height", "160px");
  jQuery(".box-owl").slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    infinite: true,
    vertical: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    verticalSwiping: true
  });

  // Build Overlay
  //adaptiveHeight: true
  var $BuildOverlay = {
    // constructor
    init: function init(source) {
      var videoID = source;
      var markup = " \n\t\t\t\t<div class=\"modal-bg\">\n\t\t\t\t\t<div class=\"modal-wrap\">\n\t\t\t\t\t\t<div class=\"modal-wrap-content\">\n\t\t\t\t\t\t\t<span class=\"close-icon\">X</span>\n\t\t\t\t\t\t\t<iframe width=\"100%\" height=\"375\" src=\"https://www.youtube.com/embed/" + source + "\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>";

      if (videoID) {
        $("body").append(markup);
      }
    }
  };

  // -- General -- //
  var $GeneralScope = {
    // Constructor
    init: function init() {
      this.menuScripts();
      this.navigationSnippet();
      this.footerCopy();
      this.mediaElelemts();
    },

    // Menu scripts
    menuScripts: function menuScripts() {
      var ButtonTrigger = $(".navbar-header .navbar-toggle");
      var MenuWrapper = $(".navbar-header .navbar-collapse");
      var NavParent = $("#navbar");

      if (ButtonTrigger) {
        $(document).on("click", ".navbar-toggle", function () {
          ButtonTrigger.toggleClass("collapsed");
          NavParent.toggleClass("collapsed");
        });
      }

      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var header_el = $(".navbar");

        if (scroll >= 100) {
          header_el.addClass("scroll_menu");
        } else {
          header_el.removeClass("scroll_menu");
        }
      });

      // Secondary menu scripts
      var $SecondaryMenuItem = $("#block-menu-menu-secundary-menu .menu li");

      $SecondaryMenuItem.eq(0).find("a").addClass("act-page");

      $SecondaryMenuItem.find("a").click(function () {
        $SecondaryMenuItem.find("a").removeClass("act-page");
        $(this).addClass("act-page");
      });
    },

    navigationSnippet: function navigationSnippet() {
      $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]').not('[href="#0"]').click(function (event) {
        // On-page links
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
          // Figure out element to scroll to
          var target = $(this.hash);

          target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $("html, body").animate({
              scrollTop: target.offset().top - $("header").outerHeight()
            }, 1000, function () {
              // Callback after animation
              // Must change focus!
              /*let $target = $(target);
              $target.focus();*/

              if ($(window).width() <= 768) {
                menu_wrapper.slideUp(300);
              }

              /*if ($target.is(":focus")) { // Checking if the target was focused
              return false;
              } else {
              $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
              };*/
            });
          }
        }
      });
    },

    footerCopy: function footerCopy() {
      var textCopy = $(".ab-inbev-footer-content-2-text-1").text();

      if ($(window).width() < 768) {
        $(".ab-inbev-footer-social-media, .ab-inbev-footer-menu").clone().appendTo("nav");

        $(".ab-inbev-footer-menu .ab-inbev-footer-menu-link:first-child").hide();
        $(".ab-inbev-footer-menu .ab-inbev-footer-menu-link:last-child a").text(textCopy);
      }
    },

    mediaElelemts: function mediaElelemts() {
      $("audio").mediaelementplayer({
        features: ["playpause", "progress", "current", "tracks", "fullscreen"]
      });
    }
  };

  // -- Agegate -- //
  var $AgegateScope = {
    // Constructor
    init: function init() {
      this.ageScripts();
    },
    // scripts for Agegate
    ageScripts: function ageScripts() {
      // Dom manipulation
      var ListCountries = $(".form-item-list-of-countries");
      var Checklist = $(".form-item-remember-me");
      var FbValidate = $(".age_checker_facebook_validate");
      var RememberMe = $(".ab-inbev-remember-me");
      var RememberLabel = Checklist.find("label");
      var RememberMeStr = RememberMe.find("strong");
      var FooterContent = $(".ab-inbev-footer");

      if (ListCountries) {
        ListCountries.insertAfter("#age_checker_error_message");
      }

      if (Checklist) {
        Checklist.append(RememberMe);
        RememberLabel.append(RememberMeStr);
      }

      if (FbValidate) {
        FbValidate.insertAfter("#edit-submit");
        FbValidate.append('<span class="fbTrigger">Sign in with <b>facebook</b></span>');
      }

      if (FooterContent) {
        $(".agegate-container-footer").append(FooterContent);
      }
    }
  };

  // -- Home -- //
  var $HomeScope = {
    // Constructor
    init: function init() {
      // Instance functions
      this.homeSliders();
      this.homeBanner();
      this.homeArticles();
    },

    // scripts for slider
    homeSliders: function homeSliders() {
      var ViewWrapper = $(".view-slider-home .view-content");
      var ViewItems = ViewWrapper.find(".views-row");

      var partnersWrapper = $(".view-id-partners_home .view-content");
      var partnersItems = partnersWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          var Instance = $(this);
          var ImageSourceDesktop = Instance.find(".home-carling-black-slider-item-image-desktop img").attr("src");
          var ImageSourceMobile = Instance.find(".home-carling-black-slider-item-image-mobile img").attr("src");

          if (ImageSourceDesktop && $(window).width() > 768) {
            Instance.css("background-image", "url(" + ImageSourceDesktop + ")");
          } else if (ImageSourceMobile && $(window).width() < 768) {
            Instance.css("background-image", "url(" + ImageSourceMobile + ")");
          }
        });
      }

      if (ViewWrapper) {
        (function () {
          var status = $(".pagination-info");
          var slick_settings = {
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
          };

          if (!ViewWrapper.hasClass("slick-initialized")) {
            ViewWrapper.on("init reInit afterChange", function (event, slick, currentSlide, nextSlide) {
              //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
              var i = (currentSlide ? currentSlide : 0) + 1;
              status.html("<b>" + i + "</b>" + " / " + slick.slideCount);
            });

            var slick_slider = ViewWrapper.slick(slick_settings);

            // Pagination
            var prevBtn = $(".slick-prev");
            var nextBtn = $(".slick-next");
            var pagWrap = $(".paginator-wrapper");

            if (pagWrap) {
              prevBtn.append('<span class="txt">PREV</span>');
              nextBtn.prepend('<span class="txt">NEXT</span>');

              pagWrap.prepend(prevBtn);
              pagWrap.append(nextBtn);
            }
          }
        })();
      }
      if (partnersWrapper) {
        var slick_settings = {
          arrows: true,
          dots: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [{
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }]
        };

        if (!partnersWrapper.hasClass("slick-initialized")) {
          partnersWrapper.slick(slick_settings);
        }
      }
    },

    // scripts for banner
    homeBanner: function homeBanner() {
      var imageWrapperDesktop = $(".home-carling-black-banner-item-image-desktop");
      var imageWrapperMobile = $(".home-carling-black-banner-item-image-mobile");

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
    },

    // Slick Articles
    homeArticles: function homeArticles() {
      // slider
      var $slick_slider = $(".view-articles-home .view-content");
      var settings_slider = {
        dots: false,
        arrows: true
      };

      slick_on_mobile($slick_slider, settings_slider);

      // slick on mobile
      function slick_on_mobile(slider, settings) {
        $(window).on("load resize", function () {
          if ($(window).width() > 767) {
            if (slider.hasClass("slick-initialized")) {
              slider.slick("unslick");
            }
            return;
          }
          if (!slider.hasClass("slick-initialized")) {
            return slider.slick(settings);
          }
        });

        var prevBtn = $(".slick-prev");
        var nextBtn = $(".slick-next");
      }
    }
  };

  // -- No Excuse -- //
  var $NoExcuseScope = {
    // Constructor
    init: function init() {
      // Instance functions
      this.excuseBanner();
      this.videoWrap();
      this.closeModal();
    },

    // scripts for banner
    excuseBanner: function excuseBanner() {
      var imageWrapperDesktop = $(".carling-black-noexcuse-banner-item-image-desktop");
      var imageWrapperMobile = $(".carling-black-noexcuse-banner-item-image-mobile");
      var heroDesktop = $("body").data("bg-desktop");
      var heroMobile = $("body").data("bg-mobile");

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css("background-image", "url(" + heroDesktop + ")");
        } else {
          $("#block-system-main").css("background-image", "url(" + heroMobile + ")");
        }
      });
    },

    videoWrap: function videoWrap() {
      var videoWrapper = $(".custom-carling-black-noexcuse-banner-1 .carling-black-noexcuse-banner-item-youtube");
      var videoId = videoWrapper.html();
      var playTrigger = $(".custom-carling-black-noexcuse-banner-1 a.play-video");

      if (videoId) {
        // Click video
        playTrigger.on("click", function (event) {
          event.preventDefault();
          /* Act on the event */
          $BuildOverlay.init(videoId);
        });
      }
    },

    // close modal instance
    closeModal: function closeModal() {
      $("body").on("click", ".modal-bg .close-icon", function (event) {
        event.preventDefault();
        /* Act on the event */
        $(".modal-bg").remove();
      });
    }
  };

  // -- Our Work -- //
  var $WorkScope = {
    // Constructor
    init: function init() {
      // Instance functions
      this.workBanners();
      this.domOurWork();
      this.workSliders();
      this.closeModal();
    },

    // scripts for banner
    workBanners: function workBanners() {
      var bannerItems = $(".our-wook-carling-black-banner-item");
      var heroDesktop = $("body").data("bg-desktop");
      var heroMobile = $("body").data("bg-mobile");
      var videoID = $("body").data("bg-youtube-video-id");

      // Build banner overlay
      var playTrigger = $("body.custom-carling-black-our-work #block-system-main a.play-video");

      playTrigger.on("click", function (event) {
        event.preventDefault();
        /* Act on the event */
        $BuildOverlay.init(videoID);
      });

      // Banner Items
      if (bannerItems) {
        bannerItems.each(function (index, el) {
          var instance = $(this);
          var videoRow = instance.data("video-youtube");
          var playTrigger = instance.find("a.play-video");
          var imageDesktop = instance.find(".our-wook-carling-black-banner-item-image-desktop");
          var imageMobile = instance.find(".our-wook-carling-black-banner-item-image-mobile");
          var imageSrc = undefined;

          $(window).on("load resize", function () {
            if ($(window).width() > 767) {
              imageSrc = imageDesktop.find("img").attr("src");
            } else {
              imageSrc = imageMobile.find("img").attr("src");
            }
            instance.css("background-image", "url(" + imageSrc + ")");
          });

          // Click video
          playTrigger.on("click", function (event) {
            event.preventDefault();
            /* Act on the event */
            $BuildOverlay.init(videoRow);
          });
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css("background-image", "url(" + heroDesktop + ")");
        } else {
          $("#block-system-main").css("background-image", "url(" + heroMobile + ")");
        }
      });
    },

    domOurWork: function domOurWork() {
      var episodesBlock = $("#block-views-episode-list-block");
      var bannerWrapper = $(".custom-carling-black-our-work-banner-2 .our-wook-carling-black-banner-item-caption");

      if (episodesBlock) {
        bannerWrapper.append(episodesBlock);
      }
    },

    // scripts for slider bottom
    workSliders: function workSliders() {
      var ViewWrapper = $(".view-slider-our-work .view-content");
      var ViewItems = ViewWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          var Instance = $(this);
          var playTrigger = Instance.find(".caption--inner > a");
          var videoRow = Instance.find(".home-carling-black-slider-item").data("video-youtube");
          var ImageSourceDesktop = Instance.find(".home-carling-black-slider-item-image-desktop img").attr("src");
          var ImageSourceMobile = Instance.find(".home-carling-black-slider-item-image-mobile img").attr("src");

          if (ImageSourceDesktop && $(window).width() > 768) {
            Instance.css("background-image", "url(" + ImageSourceDesktop + ")");
          } else if (ImageSourceMobile && $(window).width() < 768) {
            Instance.css("background-image", "url(" + ImageSourceMobile + ")");
          }

          // Click video
          playTrigger.on("click", function (event) {
            event.preventDefault();
            /* Act on the event */
            $BuildOverlay.init(videoRow);
          });
        });
      }

      if (ViewWrapper) {
        (function () {
          var status = $(".pagination-info");
          var slick_settings = {
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
          };

          if (!ViewWrapper.hasClass("slick-initialized")) {
            ViewWrapper.on("init reInit afterChange", function (event, slick, currentSlide, nextSlide) {
              //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
              var i = (currentSlide ? currentSlide : 0) + 1;
              status.html("<b>" + i + "</b>" + " / " + slick.slideCount);
            });

            var slick_slider = ViewWrapper.slick(slick_settings);

            // Pagination
            var prevBtn = $(".slick-prev");
            var nextBtn = $(".slick-next");
            var pagWrap = $(".paginator-wrapper");

            if (pagWrap) {
              prevBtn.append('<span class="txt">PREV</span>');
              nextBtn.prepend('<span class="txt">NEXT</span>');

              pagWrap.prepend(prevBtn);
              pagWrap.append(nextBtn);
            }
          }
        })();
      }
    },

    // close modal instance
    closeModal: function closeModal() {
      $("body").on("click", ".modal-bg .close-icon", function (event) {
        event.preventDefault();
        /* Act on the event */
        $(".modal-bg").remove();
      });
    }
  };

  //Take action
  var $TakeActionScope = {
    // Constructor
    init: function init() {
      // Instance functions
      this.actionBanner();
      this.customCheckbox();
      this.validateForm();
    },

    // scripts for banner
    actionBanner: function actionBanner() {
      var imageWrapperDesktop = $(".take-action-carling-black-banner-item-image-desktop");
      var imageWrapperMobile = $(".take-action-carling-black-banner-item-image-mobile");
      var heroDesktop = $("body").data("bg-desktop");
      var heroMobile = $("body").data("bg-mobile");

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          var instace = $(this);
          var imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css("background-image", "url(" + heroDesktop + ")");
        } else {
          $("#block-system-main").css("background-image", "url(" + heroMobile + ")");
        }
      });

      // Counting paragraph
      var p_count = $("p.number");
      var p_init = parseInt(p_count.attr("cdata-init"));
      var p_real = parseInt(p_count.attr("cdata-real"));

      if (p_count.length) {
        p_count.append(p_init + p_real);
      }
    },

    customCheckbox: function customCheckbox() {
      var checkboxInput = $(".form-type-checkbox label");
      var checkmark = "<span class=\"checkmark\"></span>";

      if (checkboxInput) {
        checkboxInput.each(function (index, el) {
          $(this).append(checkmark);
        });
      }
    },

    validateForm: function validateForm() {
      //Add new methods to validate fields
      jQuery.validator.addMethod("emailordomain", function (value, element) {
        return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
      }, "Please specify the correct url/email");

      jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\u00C0-\u017F" "ñ]+$/i.test(value);
      }, "Letters and spaces only please");

      if ($("#webform-client-form-111").length > 0) {
        // jQuery validate
        $("#webform-client-form-111").validate({
          rules: {
            "submitted[name]": {
              required: true
            },
            "submitted[surname]": {
              required: true
            },
            "submitted[country]": {
              required: true
            },
            "submitted[email]": {
              required: true,
              email: true,
              emailordomain: true
            },
            "submitted[mobile_number]": {
              required: true,
              number: true
            },
            "submitted[dd_mm_yyyy]": {
              required: true
            }
          },
          errorPlacement: function errorPlacement() {
            return false;
          }
        });

        $("#webform-client-form-111").submit(function (event) {
          if ($("#webform-client-form-111").valid()) {
            dataLayer.push({
              event: "Register",
              eventCategory: "Register",
              eventAction: "Sent",
              eventLabel: $("#edit-submitted-email").val()
            });
            if ($("#edit-submitted-checkboxes-1").is(":checked")) {
              window.open("/sites/g/files/phfypu206/f/Carling_GBVDigital_Booklet_Final_20180903.zip");
            }
          }
        });
      }
    }
  };

  // ----------------------------
  // TRIGGERS
  // ----------------------------

  // Trigger
  $GeneralScope.init();

  // Agegate

  if ($("body").hasClass("page-agegate")) {
    $AgegateScope.init();
  }

  $("#post-form").submit(function (event) {
    location.href = "https://www.change.org/p/homeaffairssa-update-south-africa-s-wedding-vows-for-grooms-to-take-a-vow-against-gender-based-violence-noexcuse-blacklabelsa";
  });

  // Home Scripts
  if ($("body").hasClass("front")) {
    $HomeScope.init();
  }

  // No Excuse Scripts
  if ($("body").hasClass("custom-carling-black-noexcuse")) {
    $NoExcuseScope.init();
  }

  // Our Work
  if ($("body").hasClass("custom-carling-black-our-work")) {
    $WorkScope.init();
  }

  // take action
  if ($("body").hasClass("custom-carling-black-take-action")) {
    $TakeActionScope.init();
  }
});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);
module.exports = __webpack_require__(1);


/***/ })
/******/ ]);