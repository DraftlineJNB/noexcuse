jQuery(document).ready(function ($) {
  //Landing No excuses
  jQuery("select").niceSelect();
  jQuery(".box-owl .item").css("height", "160px");
  jQuery(".box-owl").slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    infinite: true,
    vertical: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    verticalSwiping: true,
    //adaptiveHeight: true
  });

  // Build Overlay
  const $BuildOverlay = {
    // constructor
    init: function (source) {
      let videoID = source;
      let markup = ` 
				<div class="modal-bg">
					<div class="modal-wrap">
						<div class="modal-wrap-content">
							<span class="close-icon">X</span>
							<iframe width="100%" height="375" src="https://www.youtube.com/embed/${source}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>`;

      if (videoID) {
        $("body").append(markup);
      }
    },
  };

  // -- General -- //
  const $GeneralScope = {
    // Constructor
    init: function () {
      this.menuScripts();
      this.navigationSnippet();
      this.footerCopy();
      this.mediaElelemts();
    },

    // Menu scripts
    menuScripts: function () {
      let ButtonTrigger = $(".navbar-header .navbar-toggle");
      let MenuWrapper = $(".navbar-header .navbar-collapse");
      let NavParent = $("#navbar");

      if (ButtonTrigger) {
        $(document).on("click", ".navbar-toggle", function () {
          ButtonTrigger.toggleClass("collapsed");
          NavParent.toggleClass("collapsed");
        });
      }

      $(window).scroll(function () {
        let scroll = $(window).scrollTop();
        let header_el = $(".navbar");

        if (scroll >= 100) {
          header_el.addClass("scroll_menu");
        } else {
          header_el.removeClass("scroll_menu");
        }
      });

      // Secondary menu scripts
      let $SecondaryMenuItem = $("#block-menu-menu-secundary-menu .menu li");

      $SecondaryMenuItem.eq(0).find("a").addClass("act-page");

      $SecondaryMenuItem.find("a").click(function () {
        $SecondaryMenuItem.find("a").removeClass("act-page");
        $(this).addClass("act-page");
      });
    },

    navigationSnippet: function () {
      $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, "") ==
              this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
          ) {
            // Figure out element to scroll to
            let target = $(this.hash);

            target = target.length
              ? target
              : $("[name=" + this.hash.slice(1) + "]");
            // Does a scroll target exist?
            if (target.length) {
              // Only prevent default if animation is actually gonna happen
              event.preventDefault();
              $("html, body").animate(
                {
                  scrollTop: target.offset().top - $("header").outerHeight(),
                },
                1000,
                function () {
                  // Callback after animation
                  // Must change focus!
                  /*let $target = $(target);
							$target.focus();*/

                  if ($(window).width() <= 768) {
                    menu_wrapper.slideUp(300);
                  }

                  /*if ($target.is(":focus")) { // Checking if the target was focused
								return false;
							} else {
							$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
								$target.focus(); // Set focus again
							};*/
                }
              );
            }
          }
        });
    },

    footerCopy: function () {
      let textCopy = $(".ab-inbev-footer-content-2-text-1").text();

      if ($(window).width() < 768) {
        $(".ab-inbev-footer-social-media, .ab-inbev-footer-menu")
          .clone()
          .appendTo("nav");

        $(
          ".ab-inbev-footer-menu .ab-inbev-footer-menu-link:first-child"
        ).hide();
        $(".ab-inbev-footer-menu .ab-inbev-footer-menu-link:last-child a").text(
          textCopy
        );
      }
    },

    mediaElelemts: function () {
      $("audio").mediaelementplayer({
        features: ["playpause", "progress", "current", "tracks", "fullscreen"],
      });
    },
  };

  // -- Agegate -- //
  const $AgegateScope = {
    // Constructor
    init: function () {
      this.ageScripts();
    },
    // scripts for Agegate
    ageScripts: function () {
      // Dom manipulation
      let ListCountries = $(".form-item-list-of-countries");
      let Checklist = $(".form-item-remember-me");
      let FbValidate = $(".age_checker_facebook_validate");
      let RememberMe = $(".ab-inbev-remember-me");
      let RememberLabel = Checklist.find("label");
      let RememberMeStr = RememberMe.find("strong");
      let FooterContent = $(".ab-inbev-footer");

      if (ListCountries) {
        ListCountries.insertAfter("#age_checker_error_message");
      }

      if (Checklist) {
        Checklist.append(RememberMe);
        RememberLabel.append(RememberMeStr);
      }

      if (FbValidate) {
        FbValidate.insertAfter("#edit-submit");
        FbValidate.append(
          '<span class="fbTrigger">Sign in with <b>facebook</b></span>'
        );
      }

      if (FooterContent) {
        $(".agegate-container-footer").append(FooterContent);
      }
    },
  };

  // -- Home -- //
  const $HomeScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.homeSliders();
      this.homeBanner();
      this.homeArticles();
    },

    // scripts for slider
    homeSliders: function () {
      let ViewWrapper = $(".view-slider-home .view-content");
      let ViewItems = ViewWrapper.find(".views-row");

      let partnersWrapper = $(".view-id-partners_home .view-content");
      let partnersItems = partnersWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          let Instance = $(this);
          let ImageSourceDesktop = Instance.find(
            ".home-carling-black-slider-item-image-desktop img"
          ).attr("src");
          let ImageSourceMobile = Instance.find(
            ".home-carling-black-slider-item-image-mobile img"
          ).attr("src");

          if (ImageSourceDesktop && $(window).width() > 768) {
            Instance.css("background-image", "url(" + ImageSourceDesktop + ")");
          } else if (ImageSourceMobile && $(window).width() < 768) {
            Instance.css("background-image", "url(" + ImageSourceMobile + ")");
          }
        });
      }

      if (ViewWrapper) {
        let status = $(".pagination-info");
        let slick_settings = {
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        };

        if (!ViewWrapper.hasClass("slick-initialized")) {
          ViewWrapper.on(
            "init reInit afterChange",
            function (event, slick, currentSlide, nextSlide) {
              //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
              let i = (currentSlide ? currentSlide : 0) + 1;
              status.html("<b>" + i + "</b>" + " / " + slick.slideCount);
            }
          );

          const slick_slider = ViewWrapper.slick(slick_settings);

          // Pagination
          let prevBtn = $(".slick-prev");
          let nextBtn = $(".slick-next");
          let pagWrap = $(".paginator-wrapper");

          if (pagWrap) {
            prevBtn.append('<span class="txt">PREV</span>');
            nextBtn.prepend('<span class="txt">NEXT</span>');

            pagWrap.prepend(prevBtn);
            pagWrap.append(nextBtn);
          }
        }
      }
      if (partnersWrapper) {
        let slick_settings = {
          arrows: true,
          dots: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        };

        if (!partnersWrapper.hasClass("slick-initialized")) {
          partnersWrapper.slick(slick_settings);
        }
      }
    },

    // scripts for banner
    homeBanner: function () {
      let imageWrapperDesktop = $(
        ".home-carling-black-banner-item-image-desktop"
      );
      let imageWrapperMobile = $(
        ".home-carling-black-banner-item-image-mobile"
      );

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
    },

    // Slick Articles
    homeArticles: function () {
      // slider
      let $slick_slider = $(".view-articles-home .view-content");
      let settings_slider = {
        dots: false,
        arrows: true,
      };

      slick_on_mobile($slick_slider, settings_slider);

      // slick on mobile
      function slick_on_mobile(slider, settings) {
        $(window).on("load resize", function () {
          if ($(window).width() > 767) {
            if (slider.hasClass("slick-initialized")) {
              slider.slick("unslick");
            }
            return;
          }
          if (!slider.hasClass("slick-initialized")) {
            return slider.slick(settings);
          }
        });

        let prevBtn = $(".slick-prev");
        let nextBtn = $(".slick-next");
      }
    },
  };

  // -- No Excuse -- //
  const $NoExcuseScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.excuseBanner();
      this.videoWrap();
      this.closeModal();
    },

    // scripts for banner
    excuseBanner: function () {
      let imageWrapperDesktop = $(
        ".carling-black-noexcuse-banner-item-image-desktop"
      );
      let imageWrapperMobile = $(
        ".carling-black-noexcuse-banner-item-image-mobile"
      );
      let heroDesktop = $("body").data("bg-desktop");
      let heroMobile = $("body").data("bg-mobile");

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroDesktop + ")"
          );
        } else {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroMobile + ")"
          );
        }
      });
    },

    videoWrap: function () {
      let videoWrapper = $(
        ".custom-carling-black-noexcuse-banner-1 .carling-black-noexcuse-banner-item-youtube"
      );
      let videoId = videoWrapper.html();
      let playTrigger = $(
        ".custom-carling-black-noexcuse-banner-1 a.play-video"
      );

      if (videoId) {
        // Click video
        playTrigger.on("click", function (event) {
          event.preventDefault();
          /* Act on the event */
          $BuildOverlay.init(videoId);
        });
      }
    },

    // close modal instance
    closeModal: function () {
      $("body").on("click", ".modal-bg .close-icon", function (event) {
        event.preventDefault();
        /* Act on the event */
        $(".modal-bg").remove();
      });
    },
  };

  // -- Our Work -- //
  const $WorkScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.workBanners();
      this.domOurWork();
      this.workSliders();
      this.closeModal();
    },

    // scripts for banner
    workBanners: function () {
      let bannerItems = $(".our-wook-carling-black-banner-item");
      let heroDesktop = $("body").data("bg-desktop");
      let heroMobile = $("body").data("bg-mobile");
      let videoID = $("body").data("bg-youtube-video-id");

      // Build banner overlay
      let playTrigger = $(
        "body.custom-carling-black-our-work #block-system-main a.play-video"
      );

      playTrigger.on("click", function (event) {
        event.preventDefault();
        /* Act on the event */
        $BuildOverlay.init(videoID);
      });

      // Banner Items
      if (bannerItems) {
        bannerItems.each(function (index, el) {
          let instance = $(this);
          let videoRow = instance.data("video-youtube");
          let playTrigger = instance.find("a.play-video");
          let imageDesktop = instance.find(
            ".our-wook-carling-black-banner-item-image-desktop"
          );
          let imageMobile = instance.find(
            ".our-wook-carling-black-banner-item-image-mobile"
          );
          let imageSrc;

          $(window).on("load resize", function () {
            if ($(window).width() > 767) {
              imageSrc = imageDesktop.find("img").attr("src");
            } else {
              imageSrc = imageMobile.find("img").attr("src");
            }
            instance.css("background-image", "url(" + imageSrc + ")");
          });

          // Click video
          playTrigger.on("click", function (event) {
            event.preventDefault();
            /* Act on the event */
            $BuildOverlay.init(videoRow);
          });
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroDesktop + ")"
          );
        } else {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroMobile + ")"
          );
        }
      });
    },

    domOurWork: function () {
      let episodesBlock = $("#block-views-episode-list-block");
      let bannerWrapper = $(
        ".custom-carling-black-our-work-banner-2 .our-wook-carling-black-banner-item-caption"
      );

      if (episodesBlock) {
        bannerWrapper.append(episodesBlock);
      }
    },

    // scripts for slider bottom
    workSliders: function () {
      let ViewWrapper = $(".view-slider-our-work .view-content");
      let ViewItems = ViewWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          let Instance = $(this);
          let playTrigger = Instance.find(".caption--inner > a");
          let videoRow = Instance.find(".home-carling-black-slider-item").data(
            "video-youtube"
          );
          let ImageSourceDesktop = Instance.find(
            ".home-carling-black-slider-item-image-desktop img"
          ).attr("src");
          let ImageSourceMobile = Instance.find(
            ".home-carling-black-slider-item-image-mobile img"
          ).attr("src");

          if (ImageSourceDesktop && $(window).width() > 768) {
            Instance.css("background-image", "url(" + ImageSourceDesktop + ")");
          } else if (ImageSourceMobile && $(window).width() < 768) {
            Instance.css("background-image", "url(" + ImageSourceMobile + ")");
          }

          // Click video
          playTrigger.on("click", function (event) {
            event.preventDefault();
            /* Act on the event */
            $BuildOverlay.init(videoRow);
          });
        });
      }

      if (ViewWrapper) {
        let status = $(".pagination-info");
        let slick_settings = {
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        };

        if (!ViewWrapper.hasClass("slick-initialized")) {
          ViewWrapper.on(
            "init reInit afterChange",
            function (event, slick, currentSlide, nextSlide) {
              //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
              let i = (currentSlide ? currentSlide : 0) + 1;
              status.html("<b>" + i + "</b>" + " / " + slick.slideCount);
            }
          );

          const slick_slider = ViewWrapper.slick(slick_settings);

          // Pagination
          let prevBtn = $(".slick-prev");
          let nextBtn = $(".slick-next");
          let pagWrap = $(".paginator-wrapper");

          if (pagWrap) {
            prevBtn.append('<span class="txt">PREV</span>');
            nextBtn.prepend('<span class="txt">NEXT</span>');

            pagWrap.prepend(prevBtn);
            pagWrap.append(nextBtn);
          }
        }
      }
    },

    // close modal instance
    closeModal: function () {
      $("body").on("click", ".modal-bg .close-icon", function (event) {
        event.preventDefault();
        /* Act on the event */
        $(".modal-bg").remove();
      });
    },
  };

  //Take action
  const $TakeActionScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.actionBanner();
      this.customCheckbox();
      this.validateForm();
    },

    // scripts for banner
    actionBanner: function () {
      let imageWrapperDesktop = $(
        ".take-action-carling-black-banner-item-image-desktop"
      );
      let imageWrapperMobile = $(
        ".take-action-carling-black-banner-item-image-mobile"
      );
      let heroDesktop = $("body").data("bg-desktop");
      let heroMobile = $("body").data("bg-mobile");

      if (imageWrapperDesktop) {
        imageWrapperDesktop.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }
      if (imageWrapperMobile) {
        imageWrapperMobile.each(function (index, el) {
          let instace = $(this);
          let imageSrc = instace.find("img").attr("src");
          instace.css("background-image", "url(" + imageSrc + ")");
        });
      }

      // Hero banners
      $(window).on("load resize", function () {
        if ($(window).width() > 767) {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroDesktop + ")"
          );
        } else {
          $("#block-system-main").css(
            "background-image",
            "url(" + heroMobile + ")"
          );
        }
      });

      // Counting paragraph
      let p_count = $("p.number");
      let p_init = parseInt(p_count.attr("cdata-init"));
      let p_real = parseInt(p_count.attr("cdata-real"));

      if (p_count.length) {
        p_count.append(p_init + p_real);
      }
    },

    customCheckbox: function () {
      let checkboxInput = $(".form-type-checkbox label");
      let checkmark = `<span class="checkmark"></span>`;

      if (checkboxInput) {
        checkboxInput.each(function (index, el) {
          $(this).append(checkmark);
        });
      }
    },

    validateForm: function () {
      //Add new methods to validate fields
      jQuery.validator.addMethod(
        "emailordomain",
        function (value, element) {
          return (
            this.optional(element) ||
            /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) ||
            /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(
              value
            )
          );
        },
        "Please specify the correct url/email"
      );

      jQuery.validator.addMethod(
        "lettersonly",
        function (value, element) {
          return (
            this.optional(element) ||
            /^[A-Za-z\u00C0-\u017F" "ñ]+$/i.test(value)
          );
        },
        "Letters and spaces only please"
      );

      if ($("#webform-client-form-111").length > 0) {
        // jQuery validate
        $("#webform-client-form-111").validate({
          rules: {
            "submitted[name]": {
              required: true,
            },
            "submitted[surname]": {
              required: true,
            },
            "submitted[country]": {
              required: true,
            },
            "submitted[email]": {
              required: true,
              email: true,
              emailordomain: true,
            },
            "submitted[mobile_number]": {
              required: true,
              number: true,
            },
            "submitted[dd_mm_yyyy]": {
              required: true,
            },
          },
          errorPlacement: function () {
            return false;
          },
        });

        $("#webform-client-form-111").submit(function (event) {
          if ($("#webform-client-form-111").valid()) {
            dataLayer.push({
              event: "Register",
              eventCategory: "Register",
              eventAction: "Sent",
              eventLabel: $("#edit-submitted-email").val(),
            });
            if ($("#edit-submitted-checkboxes-1").is(":checked")) {
              window.open(
                "/sites/g/files/phfypu206/f/Carling_GBVDigital_Booklet_Final_20180903.zip"
              );
            }
          }
        });
      }
    },
  };

  // ----------------------------
  // TRIGGERS
  // ----------------------------

  // Trigger
  $GeneralScope.init();

  // Agegate

  if ($("body").hasClass("page-agegate")) {
    $AgegateScope.init();
  }

  $("#post-form").submit(function (event) {
    location.href =
      "https://www.change.org/p/homeaffairssa-update-south-africa-s-wedding-vows-for-grooms-to-take-a-vow-against-gender-based-violence-noexcuse-blacklabelsa";
  });

  // Home Scripts
  if ($("body").hasClass("front")) {
    $HomeScope.init();
  }

  // No Excuse Scripts
  if ($("body").hasClass("custom-carling-black-noexcuse")) {
    $NoExcuseScope.init();
  }

  // Our Work
  if ($("body").hasClass("custom-carling-black-our-work")) {
    $WorkScope.init();
  }

  // take action
  if ($("body").hasClass("custom-carling-black-take-action")) {
    $TakeActionScope.init();
  }
});
